#include <iostream>	// Remplace stdio.h
#include <cstdlib>	// Remplace stdlib.h
#include <ctime>	// Remplace time.h
#include "Object.h"

using namespace std;	// Permet d'utiliser l'espace de nom standart


class ObjetMesurable
{
public:
	virtual double longueur() const = 0;
	
};
/**
 * Création de la classe
 */

class EchSonore : public Object, public ObjetMesurable
{
	private:
		int mTaille;
		int mFrequence;
		short *mValeurs;
	public:
		// Prototype du Constructeur

		EchSonore(int longueur,int frequence);

		// Prototype du Destructeur

		virtual ~EchSonore();	

		// Accesseurs en lecture

		int frequence() const;	// const permet de dire que la méthode ne va pas modifier l'état de l'objet
		int longueur() const;
		virtual short getData(int index) const;
		
		// Accesseurs en écriture / mutateur

		short setData(int index,int valeur);

};

EchSonore::EchSonore(int longueur,int frequence) : mTaille(longueur), mFrequence(frequence)	// Constructeur
{
	mValeurs = new short[longueur]; // Allocation Dynamique, il faut utiliser l'opérateur new
	cout << "Constructeur" << endl; // endl c'est pour finir la ligne
}

EchSonore::~EchSonore()	// Destructeur
{
	// delete variable
	// ou delete [] si on veut effacer un tableau (pointeur)
	delete [] mValeurs;
	cout << "Destructeur" << endl;
}

int EchSonore::frequence() const {return mFrequence;}

int EchSonore::longueur() const {return mTaille;}

short EchSonore::getData(int index) const
{
	return mValeurs[index];
}

short EchSonore::setData(int index,int valeur)
{
	short valeurRetour = 0;

	if(valeur < -32768)
		valeurRetour = -32768;

	else if(valeur > 32767)
		valeurRetour = 32767;

	else
		valeurRetour = (short)valeur;

	mValeurs[index] = valeurRetour;

	return valeurRetour;
}

class EchSonoreMieux : public EchSonore
{
	public:
		EchSonoreMieux(int l,int f) : EchSonore(l,f){}
		virtual short getData(int index) const;

};

short EchSonoreMieux::getData(int index) const
{
	return index < 0 ? -314 : (index >= this->longueur() ? 2718 : EchSonore::getData(index));
}

void afficheEchSonore(EchSonore *echs)
{
	echs->retain();
	cout << "compteur  echs : " << echs->compte() << endl << "Global : " << Object::compteGlobal() << endl;
	//	Affichage des valeurs
	for(int index = -1; index <= echs->longueur(); index++)
		cout << "Case : " << index << " = " << echs->getData(index) << endl;
	echs->release();
}

int main(int argc, char const *argv[])
{
	srandom(time(NULL));
	int taille;
	cout << "Veuillez rentrer la taille du tableau: ";
	cin >> taille;

	cout << "Global : " << Object::compteGlobal() << endl;

	EchSonore *echs = new EchSonoreMieux(taille,44100);

	cout << "compteur  echs : " << echs->compte() << endl << "Global : " << Object::compteGlobal() << endl;
	
	for(int index = 0; index < echs->longueur(); index++)
		echs->setData(index,random()%100000-50000);

	afficheEchSonore(echs);

	cout << "compteur  echs : " << echs->compte() << endl << "Global : " << Object::compteGlobal() << endl;
	
	echs->release();
	cout << "Global : " << Object::compteGlobal() << endl;

	return 0;
}