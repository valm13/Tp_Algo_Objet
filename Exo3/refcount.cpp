#include <iostream>	// Remplace stdio.h
#include <cstdlib>	// Remplace stdlib.h

using namespace std;

class Base
{

private:
	int compteur;

public:
		// Prototypes des fonctions
	Base();
	~Base();
	void retain();
	void release();
	void afficheCpt();
};

// Code des fonctions
Base::Base()
{
	compteur = 1;
	cout << "Creating Object : ";
	this->afficheCpt();
}

void Base::retain()
{	if(this->compteur !=0){
	cout << "Retaining Object : ";
	this->compteur++;
	this->afficheCpt();
	}
	else
		cout << "Object can't be retain, because the Object had been delete" << endl;
}
void Base::release()
{
	if(this->compteur !=0){
	cout << "Releasing Object : ";
	this->compteur--;
	this->afficheCpt();
	if(this->compteur == 0)
		delete this;
	}
	else
		cout << "Object can't be release, it had already been delete" << endl;

}
void Base::afficheCpt()
{
	cout << "cpt = " << this->compteur << endl;
}
Base::~Base()
{
	cout << "Destructing Object" << endl;
}



int main(int argc, char const *argv[])
{
	Base *obj = new Base;
	obj->retain();
	obj->retain();
	obj->release();
	obj->release();
	obj->release();
	obj->release();
	obj->release();
	obj->release();
	obj->retain();


	return 0;
}