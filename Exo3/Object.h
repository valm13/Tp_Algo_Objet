#ifndef OBJECT_H
#define OBJECT_H

class Object
{
private:
	int compteur;
	static int compteurGlobal;
	public:
		Object();
		void retain();
		void release();
		int compte() const;
		static int compteGlobal();
		virtual ~Object();
};

#endif