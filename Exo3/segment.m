@implementation Segment
-initAvecPositionX1:(double)X1 Y1:(double)Y1 X2:(double)X2 Y2:(double)Y2
{
	if((self = [super init]) != nil)
	{
		[self fixePositionX1:X1 Y1:Y1 X2:X2 Y2:Y2];
	}
	return self;
}
-init
{
	[self initAvecPositionX1:NAN Y1:NAN X2:NAN Y2:NAN];
	return self;
}
-fixePositionX1:(double)fixeX1 Y1:(double)fixeY1 X2:(double)fixeX2 Y2:(double)fixeY2
{
	self->x1 = fixeX1;
	self->y1 = fixeY1;
	self->x2 = fixeX2;
	self->y2 = fixeY2;
	return self;
}
-(double)longueur
{
	return (double)((self->y2 - self->y1)/(self->x2 - self->x1));
}
@end