#include <iostream>	// Remplace stdio.h
#include <cstdlib>	// Remplace stdlib.h
#include "Object.h"
// CORRECTION
using namespace std;

int Object::compteurGlobal = 0;

Object::Object() : compteur(1)
{
	++compteurGlobal;
	cout << "Construction of an Object" << endl;
}

void Object::retain()
{
	++compteur;
	++compteurGlobal;
}

void Object::release()
{
	--compteurGlobal;
	--compteur;
	if(compteur == 0) delete this;
}

int Object::compte()const{return compteur;}

int Object::compteGlobal(){return compteurGlobal;}

Object::~Object() {cout << "Destructor of an Object" << endl;}