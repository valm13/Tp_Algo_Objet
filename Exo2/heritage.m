#import <Foundation/Foundation.h>
#include <stdio.h>

/**	OBJET A **/

@interface ObjetA : NSObject
{}
-init;
-methode;
@end

@implementation ObjetA
-init
{
	if ((self = [super init]) != nil)
	{
		puts("init A");
		[self methode];
	}
	return self;
}

-methode
{
	puts("methode d'ObjetA");
	return self;
}
@end

/**	OBJET B **/

@interface ObjetB : ObjetA
{}
-init;
-methode;
@end

@implementation ObjetB
-init
{
	if ((self = [super init]) != nil)	// [super init] nous ramenne à l'init de objet A puis le self methode renverra celle de objet B car on est un objet B
	{
		puts("init B");
		[self methode];
	}
	return self;
}

-methode
{
	puts("methode d'ObjetB");
	return self;
}
@end

int main(int argc, char const *argv[])
{
	ObjetA *machin = [[ObjetB alloc] init];	// [ObjetB alloc] crée un objet B puis le init sera celui de objet B
	[machin methode];
	[machin release];
	return 0;
}