#include <cstdio>

using namespace std;

class ObjetA
{
public:
	ObjetA()
	{
		puts("Init A");
		methode();
	}
	virtual void methode()
	{
		puts("Methode d'ObjetA");
	}
	virtual ~ObjetA(){}		// Si on a au moins une méthode polimorphique (virtual), il faut un destructeur lui aussi virtual.
};

class ObjetB : public ObjetA
{
public:
	ObjetB()
	{
		puts("Init B");
		methode();
	}
	virtual void methode()	// virtual permet de dire que cette méthode doit être appelée pour un Objet B
	{						
		puts("Methode d'ObjetB");
	}
};

int main(int argc, char const *argv[])
{
	ObjetA *obj = new ObjetB;
	obj->methode();
	delete obj;

	return 0;
}