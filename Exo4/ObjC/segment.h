#import <Foundation/Foundation.h>
#import "objetmesurable.h"
#import "point2D.h"


@interface Segment : NSObject<ObjetMesurable>
{
	Point2D *startpoint,*endpoint;

}
-initAvecPositionP1:(Point2D*)p1 P2:(Point2D*)p2;

-init;

-fixePositionStartPoint:(Point2D*)p1New EndPoint:(Point2D*)p2New;

-(double)longueur;
-dealloc;
@end