#import <Foundation/Foundation.h>
#import "objetmesurable.h"
#include <string.h>
#include <math.h>
#include "point2D.h"
#include "chaine.h"
#include "segment.h"

#define TAILLE 3
#define NBPOINTS 3


//	Implémentation de Chaine	

@implementation Chaine

-initAvec:(const char*)chaineInitiale
{
	if((self = [super init]) != nil)
	{
		[self fixeChaine:chaineInitiale];
	}
	return self;
}

-init
{
	return [self initAvec:""];
}

-fixeChaine:(const char*)nouvelleChaine
{
	chaine = nouvelleChaine;
	return self;
}
-(const char*)chaine
{
	return chaine;
}
-(double)longueur
{
	int i = 0;
	if(chaine != NULL)
		while(chaine[i] != '\0')
			i++;
		return i;
	// return (double)strlen(chaine);
}
@end


//	Implémentation de Segment	

@implementation Segment
// Sans Point2D
// 
// -initAvecPositionX1:(double)X1 Y1:(double)Y1 X2:(double)X2 Y2:(double)Y2
// {
// 	if((self = [super init]) != nil)
// 	{
// 		[self fixePositionX1:X1 Y1:Y1 X2:X2 Y2:Y2];
// 	}
// 	return self;
// }
// -init
// {
// 	[self initAvecPositionX1:NAN Y1:NAN X2:NAN Y2:NAN];
// 	return self;
// }
// -fixePositionX1:(double)fixeX1 Y1:(double)fixeY1 X2:(double)fixeX2 Y2:(double)fixeY2
// {
// 	self->x1 = fixeX1;
// 	self->y1 = fixeY1;
// 	self->x2 = fixeX2;
// 	self->y2 = fixeY2;
// 	return self;
// }
// -(double)longueur
// {
// 	return (double)((self->y2 - self->y1)/(self->x2 - self->x1));
// 	return sqrt((self->y2-self->y1)(self->y2-self->y1)+(self-x2-self->x1)(self->x2-self->x1))
// }

-initAvecPositionP1:(Point2D*)p1 P2:(Point2D*)p2
{
	if((self = [super init]) != nil)
	{
		[self fixePositionStartPoint:p1 EndPoint:p2];
	}
	return self;
}

-init
{
	return [self initAvecPositionP1:nil P2:nil];
}

-fixePositionStartPoint:(Point2D*)p1New EndPoint:(Point2D*)p2New
{
	// self->startpoint = p1New; // pas trés bon
	// self->endpoint = p2New; // pas trés bon
	
	// permet de ne pas avoir de problèmes de réallocation
	// version 1:
	// if(p1 != p1New)
	// {
	// 	[p1 release];
	// 	[p1New retain];
	// 	startpoint = p1New;
	// }
	// if(p2 != p2New)
	// {
	// 	[p2 release];
	// 	[p2New retain];
	// 	endpoint = p2New;
	// }
	// version 2:
		[p1New retain];
		[startpoint release];
		startpoint = p1New;

		[p2New retain];
		[endpoint release];
		endpoint = p2New;
	return self;
}

-(double)longueur
{
	double x1 = [startpoint getX];
	double x2 = [endpoint getX];

	double y1 = [startpoint getY];
	double y2 = [endpoint getY];
	printf("x1=%f|x2=%f|y1=%f|y2=%f\n",x1,x2,y1,y2);

	return sqrt((x1-x2)*(x1-x2) + (y1-y2)*(y1-y2));
}

-dealloc
{
	[startpoint release];
	[endpoint release];
	[super dealloc];
	return self;
}

@end


//	Implémentation de Point2D	

@implementation Point2D
-initAvecCoordX:(double)X Y:(double)Y
{
	if((self = [super init]) != nil)
	{
		[self fixePointX:X Y:Y];
	}
	return self;
}
-init
{
	[self initAvecCoordX:NAN Y:NAN];
	return self;
}
-fixePointX:(double)fixeX Y:(double)fixeY
{
	x = fixeX;
	y = fixeY;
	return self;
}
-(double)getX{return self->x;}
-(double)getY{return self->y;}
@end

double sommeLongueurs(id<ObjetMesurable> *objets, int nombre)	// Id correspond à un pointeur générique sur l'objet ex : *Objet, l'autre * correspond à un tableau
{
	double somme = 0;
	int index;
	for(index = 0;index < nombre;index++)
		somme += [objets[index] longueur];
	return somme;
}

int main(int argc, char const *argv[])
{


	// Partie 1

	// Chaine *chaine = [[Chaine alloc] init];

	// printf("Chaine initiale : %s\n",[chaine chaine]);
	// printf("Chaine choisie : %s\n",[[chaine fixeChaine:"coucou"] chaine]);

	// [chaine release];

	// Partie 2
	
	// OLD VERSION of SEGMENT : objet[4] = [[Segment alloc] initAvecPositionX1:0 Y1:1 X2:1 Y2:70.32];
	NSObject<ObjetMesurable> * objet[TAILLE];
	int index;
	Point2D *point[NBPOINTS];
	point[0] = [[Point2D alloc] initAvecCoordX:0 Y: 0];
	point[1] = [[Point2D alloc] initAvecCoordX:1 Y: 1];
	point[2] = [[Point2D alloc] initAvecCoordX:2 Y: 2];

	// objet[0] = [[Chaine alloc] initAvec:"Serpentin"];
	// objet[1] = [[Chaine alloc] initAvec:"lol"];
	// objet[2] = [[Chaine alloc] initAvec:"c'est"];
	// objet[3] = [[Chaine alloc] initAvec:"une"];
	objet[0] = [[Segment alloc] initAvecPositionP1:point[0] P2:point[1]];
	objet[1] = [[Segment alloc] initAvecPositionP1:point[1] P2:point[2]];
	objet[2] = [[Segment alloc] initAvecPositionP1:point[2] P2:point[0]];


	printf("Somme des longueurs des %d objet : %f\n",TAILLE,sommeLongueurs(objet, TAILLE));
	for(index = 0; index < NBPOINTS; index++)
		[point[index] release];
	for(index = 0; index < TAILLE; index++)
		[objet[index] release];


	return 0;
}