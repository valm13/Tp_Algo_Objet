#import <Foundation/Foundation.h>
#import "objetmesurable.h"

@interface Chaine : NSObject<ObjetMesurable>
{
	const char *chaine;
}

-init;

-initAvec:(const char*)chaineInitiale;

-fixeChaine:(const char*)nouvelleChaine;

-(const char*)chaine;

-(double)longueur;
@end
