#import <Foundation/Foundation.h>


@interface Point2D : NSObject
{
	double x,y;

}
-initAvecCoordX:(double)X Y:(double)Y;

-init;

-fixePointX:(double)fixeX Y:(double)fixeY;

-(double)getX;
-(double)getY;
@end