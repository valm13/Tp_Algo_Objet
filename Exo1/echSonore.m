#import <Foundation/Foundation.h> // Le import fait un include une seule fois même si on le refais on le fait our les librairies spécifiques à l'Objective C
#include <stdlib.h>
#include <time.h>
#include <stdio.h>




// On ne frabrique pas l'objet, à partir de rien
@interface EchSonore : NSObject	// Comment on construit l'objet (NS = NextStep bibliotheque racheté par Apple)
{
	short *valeurs;
	int longueur;
	int frequence;
}
-initWithLongueur:(int)longueur frequence:(int)frequence;	// initWithLongueur:frequence:
-(int)frequence;
-(int)longueur;
-(short)getData:(int)index;	//getData
-(short)setData:(int)valeur en :(int)indice;	// setData:en: => setData:12 en :10
-(short)fixeEn:(int)indice valeur:(int)valeur;	// fixeEn:valeur: => fixeEn:10 valeur: 12
-(short)setData:(int)valeur :(int)indice;	// setData:: => setData:12 :10
-dealloc;
@end



@implementation EchSonore
-initWithLongueur:(int)l frequence:(int)f
{
	// Pour utiliser un objet, on doit initialiser l'objet dont il hérite
	// on envois à super le message init (super c'est la classe dont EchSonore hérite ici NSOBJECT)
	self = [super init]; // self pour savoir si on peut initialiser
	if(self != NULL)
	{
		self->longueur = l;	// ??????
		self->frequence = f;	// ??????
		self->valeurs = (short *)malloc(sizeof(short)*longueur);
	}
	return self;
}
-(int)frequence{return frequence;}
-(int)longueur{return longueur;}
-(short)getData:(int)index{return valeurs[index];}
-(short)setData:(int)valeur en :(int)indice
{
	short valeurRetour = 0;

	if(valeur < -32768)
		valeurRetour = -32768;

	else if(valeur > 32767)
		valeurRetour = 32767;

	else
		valeurRetour = (short)valeur;

	self->valeurs[indice] = valeurRetour;

	return valeurRetour;
}
-(short)fixeEn:(int)indice valeur:(int)valeur
{
	return [self setData:valeur en:indice];
}
-(short)setData:(int)valeur :(int)indice
{
	return [self setData:valeur en:indice];
}
-dealloc
{
	free(valeurs);
	[super dealloc];	// Nettoyage des fondations
	return self;
}
@end




@interface EchSonoreMieux : EchSonore // Qui hérite de EchSonore	
{}	// Pas de variables membre
-(short)getData:(int)index;	// on redéfinit getData:
-(void)coucou;
@end

@implementation EchSonoreMieux
-(short)getData:(int)index
{
	short valeurRetour;
	if(index < 0)
		valeurRetour = -314;	// 0
	else if (index >= [self longueur])
		valeurRetour = 2718;	// 0
	else
		valeurRetour = [super getData:index];	// On appelle le getData d'au dessus (EchSonore)
	return valeurRetour;
}
-(void)coucou{puts("coucou");}
@end





void afficheEchSonore(EchSonore *echs)
{
	int i;
	for(i = -1; i <= [echs longueur]; ++i)	// ou i++ c  presque pareil
		printf("Case [%d] = %hd\n", i, [echs getData:i]);
}

int main(void)
{
	srandom(time(NULL));
	EchSonore *echs = [[EchSonoreMieux alloc] initWithLongueur:10 frequence:44100];
	[(EchSonoreMieux*)echs coucou];
	int i;
	for(i = 0; i < [echs longueur]; ++i)	// ou i++ c  presque pareil
		[echs setData:random()%100000-50000 en:i];
	afficheEchSonore(echs);

	[echs release];
	return 0;
}