#import <Foundation/Foundation.h>

@protocol ObjetGeom
-(double)surface;
-(double)perimetre;
@end