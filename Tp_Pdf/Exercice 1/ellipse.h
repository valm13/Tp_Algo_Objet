#ifndef DEF_ELLIPSE // Si la constante n'a pas été définie le fichier n'a jamais été inclus
#define DEF_ELLIPSE // On définit la constante pour que la prochaine fois le fichier ne soit plus inclus

#import <Foundation/Foundation.h>
#import "ObjetGeom.h"

/* Contenu de votre fichier .h (autres include, prototypes, define...) */
@interface Ellipse : NSObject<ObjetGeom>
{
	double demiGrandAxe,demiPetitAxe; // Demi grand axe et Demi petit axe
}
-init;
-initAvecDemiGrandAxe:(double)GA DemiPetitAxe:(double)PA;
-fixeDemiGrandAxe:(double)newGA DemiPetitAxe:(double)newPA;
-(double)surface;
-(double)perimetre;
@end

#endif

