#ifndef DEF_CERCLE // Si la constante n'a pas été définie le fichier n'a jamais été inclus
#define DEF_CERCLE // On définit la constante pour que la prochaine fois le fichier ne soit plus inclus

#import <Foundation/Foundation.h>
#import "ObjetGeom.h"
#include "ellipse.h"

/* Contenu de votre fichier .h (autres include, prototypes, define...) */
@interface Cercle : Ellipse<ObjetGeom>
-init;
-initAvecRayon:(double)R;
-fixeRayon:(double)newR;
-(double)surface;
-(double)perimetre;
@end

#endif
