#import <Foundation/Foundation.h>
#include <stdio.h>
#import "ObjetGeom.h"
#include "rectangle.h"
#include "carre.h"
#include "ellipse.h"
#include "cercle.h"

#define TAILLE 2


int main(int argc, char const *argv[])
{
	NSObject<ObjetGeom> * objet[TAILLE];
	int index;

	objet[0] = [[Rectangle alloc] initAvecWidth:1 Height:2];
	objet[1] = [[Carre alloc] initAvecTaille:5];

	// for(index = 0; index < TAILLE; index++)
	// 	printf("Le perimetre de l'objet %d est %f\n",index,[objet[index] perimetre]);

	for(index = 0; index < TAILLE; index++)
		[objet[index] release];

	return 0;
}