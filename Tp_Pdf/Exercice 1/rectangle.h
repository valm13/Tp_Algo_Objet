#ifndef DEF_RECTANGLE // Si la constante n'a pas été définie le fichier n'a jamais été inclus
#define DEF_RECTANGLE // On définit la constante pour que la prochaine fois le fichier ne soit plus inclus

#import <Foundation/Foundation.h>
#import "ObjetGeom.h"

/* Contenu de votre fichier .h (autres include, prototypes, define...) */
@interface Rectangle : NSObject<ObjetGeom>
{
	double width,height;
}
-init;
-initAvecWidth:(double)W Height:(double)H;
-fixeWidth:(double)newW Height:(double)newH;
-(double)surface;
-(double)perimetre;
@end

#endif

