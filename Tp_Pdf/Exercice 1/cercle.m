#import <Foundation/Foundation.h>
#import "ObjetGeom.h"
#include "cercle.h"
#include <math.h>

@implementation Cercle

-initAvecRayon:(double)R
{
	if((self = [super init]) != nil)
		[self fixeRayon:R];
	return self;
}

-init
{
	return [self initAvecRayon:NAN];
}

-fixeRayon:(double)newR
{
	demiGrandAxe = newR;
	demiPetitAxe = newR;
	return self;
}

-(double)surface
{
	double r= demiGrandAxe;
	double surface = M_PI*r*r;	// πr²
	return surface;
}

-(double)perimetre
{
	double r= demiGrandAxe;
	double perimetre = 2*M_PI*r; // 2πr
	return perimetre;
}

@end