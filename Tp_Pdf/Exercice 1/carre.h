#ifndef DEF_CARRE // Si la constante n'a pas été définie le fichier n'a jamais été inclus
#define DEF_CARRE // On définit la constante pour que la prochaine fois le fichier ne soit plus inclus

#import <Foundation/Foundation.h>
#import "ObjetGeom.h"
#include "rectangle.h"

/* Contenu de votre fichier .h (autres include, prototypes, define...) */
@interface Carre : Rectangle<ObjetGeom>
-init;
-initAvecTaille:(double)T;
-fixeTaille:(double)newT;
-(double)Cote;
-(double)surface;
-(double)perimetre;
@end

#endif

