#import <Foundation/Foundation.h>
#import "ObjetGeom.h"
#include "carre.h"


@implementation Carre

-initAvecTaille:(double)T
{
	if((self = [super init]) != nil)
		[self fixeTaille:T];
	return self;
}

-init
{
	return [self initAvecTaille:NAN];
}

-fixeTaille:(double)newT
{
	width = newT;
	height = newT;
	return self;
}

-(double)Cote;
{
	return width;
}
-(double)surface
{
	double surface = width*height;
	return surface;
}

-(double)perimetre
{
	double perimetre = 2*(width+height);
	return perimetre;
}
@end