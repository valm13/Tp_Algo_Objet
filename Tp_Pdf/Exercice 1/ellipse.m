#import <Foundation/Foundation.h>
#import "ObjetGeom.h"
#include "ellipse.h"
#include <math.h>
@implementation Ellipse

-initAvecDemiGrandAxe:(double)GA DemiPetitAxe:(double)PA
{
	if((self = [super init]) != nil)
	{
		[self fixeDemiGrandAxe:GA DemiPetitAxe:PA];
	}
	return self;
}

-init
{
	return [self initAvecDemiGrandAxe:NAN DemiPetitAxe:NAN];
}

-fixeDemiGrandAxe:(double)newGA DemiPetitAxe:(double)newPA
{
	demiGrandAxe = newGA;
	demiPetitAxe  = newPA;
	return self;
}

-(double)surface
{	
	double a = demiPetitAxe;
	double A = demiGrandAxe;
	double surface = M_PI * a * A;
	return surface;
}

-(double)perimetre
{
	//	Formula: π[3(R + r)]   - √[(3R + r)(R + 3r)]
	double a = demiPetitAxe;
	double A = demiGrandAxe;
	double perimetre = M_PI*(3*(a+A)) - sqrt((3*A + a)*(A + 3*a));
	return perimetre;
}

@end