#import <Foundation/Foundation.h>
#import "ObjetGeom.h"
#include "rectangle.h"

@implementation Rectangle
-initAvecWidth:(double)W Height:(double)H
{
	if((self = [super init]) != nil)
	{
		[self fixeWidth:W Height:H];
	}
	return self;
}

-init
{
	return [self initAvecWidth:NAN Height:NAN];
}

-fixeWidth:(double)newW Height:(double)newH
{
	width = newW;
	height = newH;
	return self;
}

-(double)surface
{
	double surface = width*height;
	return surface;
}

-(double)perimetre
{
	double perimetre = 2*(width+height);
	return perimetre;
}
@end